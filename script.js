//3.Retrieve all
fetch('https://jsonplaceholder.typicode.com/todos', {
method: 'GET' 
})
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json())
.then((data)=>console.log(data))

//5.Retrieve single item
 fetch('https://jsonplaceholder.typicode.com/todos/1') 
 .then((response)=>response.json())
.then((data)=>console.log(data))
//6.
console.log('The item "delectus aut otem" has a status of false')

//7. Create to do list using POST
 fetch('https://jsonplaceholder.typicode.com/todos', {
 method: 'POST',
 headers: {
 	'Content-Type':'application/json'
},
 body: JSON.stringify({
	title: 'Created New To do list',
 	userId: 1,
 	id: 1
 	})
})
 .then((response) => response.json())
 .then((data) =>console.log(data))

//8.PUT
fetch('https://jsonplaceholder.typicode.com/todos/2', {
method: 'PUT',
headers: {
	'Content-Type':'application/json'
	},
body: JSON.stringify({
	dateCompleted: "Pending",
	description: "To update my todo list with diffrent data stracture",
	title: 'Updated todo list',
	userId:2,
	status: "Pending"
	})
})
 .then((response) => response.json())
 .then((data) =>console.log(data))

 

 //PATCH
  fetch('https://jsonplaceholder.typicode.com/todos/2', {
method: 'PATCH',
headers: {
	'Content-Type':'application/json'
	},
body: JSON.stringify({
	dateCompleted: "03/01/22",
	description: "updated todo list with diffrent data stracture",
	title: 'delectus autem',
	userId:2,
	status: "Complete"
	})
})
 .then((response) => response.json())
 .then((data) =>console.log(data))

 //DELETE
 //[Delete post]
fetch('https://jsonplaceholder.typicode.com/todos/1', {
method: 'DELETE' 
})
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=>response.json())
.then((data)=>console.log(data))
